variable "project_id" {}

variable "credentials_file" {}

variable "user" {
  default     = "aroutioun.tsakanian"
  description = "Nom de l'utilisateur"
  type        = string
}

variable "region" {
  default = "us-east1"
  type    = string
}

variable "zone" {
  default = "us-central1-a"
  type    = string
}
